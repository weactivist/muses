import React from 'react';
import { Link } from 'react-router-dom';
import './style/latest.css';

export default class Latest extends React.Component {
    constructor() {
        super();

        this.state = {
            list: []
        };
    }

    render() {
        return (
            <div id="latest">
                <h1>Latest!</h1>
                <div className="flex flex-wrap">
                    {this.state.list.map(item => <Link to={'/article/' + item._id} key={item._id}>{item.title}</Link>)}
                </div>
            </div>
        )
    }

    componentWillMount() {
        this._fetchList();
    }

    _fetchList() {
        fetch('http://localhost:8080/articles')
            .then(response => response.json())
            .then(json => this.setState({list: json}))
            .then(state => console.log(this.state.list))
    }
}
