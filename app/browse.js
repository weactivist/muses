import React from 'react';
import './style/browse.css';

export default class Browse extends React.Component {
    constructor() {
        super();

        this.state = {
            list: []
        };
    }

    render() {
        return (
            <div id="browse">
                <h1>Hello, Electron and ReactJS!</h1>
                <div className="flex flex-wrap">
                    {this.state.list.map(item => <div key={item.id}><img src={item.thumbnailUrl} alt={item.title} /><br />{item.title}</div>)}
                </div>
            </div>
        )
    }

    componentWillMount() {
        this._fetchList();
    }

    _fetchList() {
        fetch('https://jsonplaceholder.typicode.com/photos')
            .then(response => response.json())
            .then(json => json.slice(0, 25))
            .then(json => this.setState({list: json}))
            .then(state => console.log(this.state.list));

    }
}
