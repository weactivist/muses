import React from 'react';
import './style/article.css';

export default class Article extends React.Component {
    constructor() {
        super();

        this.state = {
            article: {}
        };
    }

    render() {
        return (
            <div id="article">
                <iframe></iframe>
            </div>
        )
    }

    componentWillMount() {
        this._fetchArticle();
    }

    _initIframe(html) {
        let element = window.document.getElementsByTagName('iframe')[0];
        let document = element.document;

        if(element.contentDocument) {
            document = element.contentDocument;
        }
        else if (element.contentWindow) {
            document = element.contentWindow;
        }

        console.log(window.innerHeight);

        document.open();
        document.write(html);
        document.close();
    }

    _fetchArticle() {
        fetch('http://localhost:8080/articles/' + this.props.match.params.id)
            .then(response => response.json())
            .then(json => this.setState({article: json}))
            .then(state => this._initIframe(this.state.article.content));
    }
}
