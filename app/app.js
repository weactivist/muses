import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Menu from './menu.js';
import Browse from './browse.js';
import Discover from './discover.js';
import Latest from './latest.js';
import Article from './article.js';
import './style/app.css';
import './style/main.css';

class Root extends React.Component {
    constructor() {
        super();

        this.state = {
            page: 'browse'
        };
    }

    render() {
        return (
            <Router>
                <div className="flex">
                    <Menu changePage={this._changePage.bind(this)} />
                    <div id="main" className="container">
                        <Route exact path="/browse" component={Browse} />
                        <Route exact path="/discover" component={Discover} />
                        <Route exact path="/latest" component={Latest} />
                        <Route exact path="/article/:id" component={Article} />
                    </div>
                </div>
            </Router>
        )
    }

    _changePage(name) {
        this.setState({
            page: name
        });
    }
}

ReactDOM.render(
  <Root />,
  document.getElementById('root')
);
