import React from 'react';
import { Link } from 'react-router-dom';
import './style/menu.css';

export default class Menu extends React.Component {
    render() {
        return (
            <nav id="menu">
                <Link to="/browse">Browse</Link>
                <Link to="/discover">Discover</Link>
                <Link to="/latest">Latest</Link>

                <span>Your library</span>
                <a href="#">Starred</a>

                <span>Subscriptions</span>
                <a href="#">Sub 1</a>
                <a href="#">Sub 2</a>
                <a href="#">Sub 3</a>
                <a href="#">Sub 4</a>
            </nav>
        )
    }

    _handleClick(page) {
        this.props.changePage(page);
    }
}
